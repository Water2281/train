package com.company;

import java.util.ArrayList;
import java.util.List;

public class LuxWagon extends WagonForPassanger implements WithConditioner {
    private boolean turnOnOf;

    @Override
    public void setPlaceNum(int p) {
        super.setPlaceNum(p);
    }

    @Override
    public void addPassnager(Passanger p) {
        super.addPassnager(p);
    }

    @Override
    public void KickOutPassanger(Passanger p) {
        super.KickOutPassanger(p);
    }

    @Override
    public List<Passanger> getPassangers() {
        return super.getPassangers();
    }

    @Override
    public boolean isFull() {
        return super.isFull();
    }

    @Override
    public void turnOffAirConditioner() {
        turnOnOf=true;
    }

    @Override
    public void turnOnAirConditioner() {
        turnOnOf=false;
    }
}
