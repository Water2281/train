package com.company;

import java.util.List;

public abstract class WagonForPassanger implements PassangerWagon
{
    private int placeNum ;
    private List<Passanger> passangers;

    public void setPlaceNum(int p){
        this.placeNum=p;
    }

    public boolean isFull(){
        if(passangers.size()==10){
            return true;
        }
        return false;
    }

    public void addPassnager(Passanger p) {
        if(isFull()){
            System.out.println("Wagon is full");
        }
        else{
            passangers.add(p);
        }
    }

    public void KickOutPassanger(Passanger p){
        passangers.remove(p);
    }

    public List<Passanger> getPassangers() {
        return passangers;
    }
}
