package com.company;

import java.util.List;

public abstract class WagonForGoods implements FreightWagon{
    private int placeNum;
    private List<Good> goods;

    public void setPlaceNum(int p){
        this.placeNum = p;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public boolean isFull() {
        if(goods.size()==20){
            return true;
        }
        else{
            return false;
        }
    }
}
