package com.company;

import java.util.List;

public interface FreightWagon extends Wagon {
    List<Good> getGoods();
}
