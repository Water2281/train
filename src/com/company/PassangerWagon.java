package com.company;

import java.util.List;

public interface PassangerWagon extends Wagon{
    List<Passanger> getPassangers();
}
