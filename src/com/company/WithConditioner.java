package com.company;

public interface WithConditioner {
    void turnOffAirConditioner();
    void turnOnAirConditioner();
}
