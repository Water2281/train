package com.company;

public enum TypeOfTrain {
    PassengerTrain,
    FastTrain,
    FreightTrain
}
